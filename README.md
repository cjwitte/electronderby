# Evolving instructions for running electron derbyp app
* Eventually this is intended to be distributed as a windows msi or a mac dmg
* For now you will need node and npm installed on the machine as pre-requisites
* run the following:
```
npm install
npm run timer
```

* to test, run this command:
```
node udpSend2.js
```

